# Ban Notes to User Notes

This is a small Node.js script to automatically add ban notes for a user to their Toolbox user notes. 

### Installation

1. Install Node.js 8+. I recommend using [nvm](https://github.com/creationix/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows).
2. Run `npm install` to install the required dependencies for this project. 
3. Rename `refreshTokenSample.json` to `refreshToken.json` and fill in the missing fields with your own information. You can get a client ID and secret [here](https://reddit.com/prefs/apps), and you can use the excellent [reddit-oauth-helper](https://github.com/not-an-aardvark/reddit-oauth-helper) to get a refresh token (you only need the refresh token from this!).
4. Change the `subreddit` property in `config.json` to match your subreddit. If you're concerned about wasting API calls, you can also reduce the frequency at which the script polls the modlog. 
5. (OPTIONAL) Install `nodemon` and `tmux` for persistence. `nodemon` will restart the script in the event it crashes, while `tmux` allows you to run a separate shell that won't stop the script when you exit your current session. 
6. Run `node app.js` to start the script (or `nodemon app.js` if you're using `nodemon`). Press `Ctrl-C` to stop the script.