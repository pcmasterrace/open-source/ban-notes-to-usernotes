"use strict";

const snoowrap = require("snoowrap");
const chalk = require("chalk");
const Base64 = require("base-64");
const pako = require("pako");
const moment = require("moment");

const refreshToken = require("./refreshToken");
const config = require("./config");

const r = new snoowrap(refreshToken);

r.getMe().then(userInfo => {
	console.log(chalk`{bgGreen.bold Authentication:} {dim Successfully authenticated as} {bold /u/${userInfo.name}}`);
});

setTimeout(monitorModlog, 3000, config.subreddit);

async function monitorModlog(subreddit, lastActionTime) {
	let modActions = await r.getSubreddit(subreddit).getModerationLog({type: "banuser"});
	
	if (lastActionTime === undefined) {
		lastActionTime = moment.utc(modActions[0].created_utc * 1000);
	}
	
	modActions = modActions.filter(action => {
		return moment.utc(action.created_utc * 1000).isAfter(lastActionTime);
	});
	
	if (modActions.length > 0) {
		let usernotesWikiPage = await r.getSubreddit(subreddit).getWikiPage("usernotes");
		let usernotesWikiPageContent = await usernotesWikiPage.fetch();
		usernotesWikiPageContent = JSON.parse(usernotesWikiPageContent.content_md);
		
		// Decompress zipped usernotes from Reddit
		// Mostly taken from the Toolbox source code. This took more time to get working than I'd like to admit
		let inflate = new pako.Inflate({to:'string'});
		inflate.push(Base64.decode(usernotesWikiPageContent.blob));
		let usernotes = JSON.parse(inflate.result);
		
		modActions.forEach(action => {
			let reason = {
				n: `Ban note: ${action.description}`,
				t: action.created_utc,
				m: usernotesWikiPageContent.constants.users.indexOf(action.mod),
				l: "",
				w: 1
			};
			
			if (usernotes.hasOwnProperty(action.target_author)) {
				usernotes[action.target_author].ns.push(reason);
			} else {
				usernotes[action.target_author] = {};
				usernotes[action.target_author].ns = [reason];
			}
		});
		
		// Recompress usernotes back into zipped format.
		let deflate = new pako.Deflate({to: "string"});
		deflate.push(JSON.stringify(usernotes), true);
		usernotesWikiPageContent.blob = Base64.encode(deflate.result);
		
		// Submit changed usernotes to Reddit
		await usernotesWikiPage.edit({text: JSON.stringify(usernotesWikiPageContent), reason: `Synchronizing ban notes for ${modActions.map(action => action.target_author).join(", ").toString()}`});
		
		process.stdout.clearLine();
		process.stdout.cursorTo(0);
		console.log(chalk`{inverse.bold Action:} Synchronized ban notes for {bold ${modActions.map(action => action.target_author).join(", ").toString()}} at {bold ${moment.utc().local().format("ddd MMM D h:mm:ss A")}}`);
	}
	
	lastActionTime = moment.utc(modActions[0].created_utc * 1000);
	
	process.stdout.clearLine();
	process.stdout.cursorTo(0);
	process.stdout.write(chalk`{blue.bold Modlog monitor:} {dim Last checked} /r/${subreddit} {dim on} ${moment.utc().local().format("ddd, h:mm:ss A")}`);
	
	setTimeout(monitorModlog, config.checkInterval, subreddit, lastActionTime);
}